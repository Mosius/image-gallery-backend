import 'dotenv/config';
import mongoose from './mongodb.config';

export default {
  apiPort: process.env.API_PORT || 3002,
  apiVersion: process.env.API_VERSION || 'v1',
  authTokenSecret: process.env.AUTH_TOKEN_SECRET || 'default',
  mediaPath: process.env.MEDIA_PATH || '/home/mostafa/Projects/University/ImageGallery/photos',
  mediaUrl: `${(process.env.MEDIA_URL || 'http://localhost:3002')}/uploads`,
  pageSize: process.env.PAGE_SIZE || 10,
  mongoose,
};
