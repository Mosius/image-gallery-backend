import jwt from 'jsonwebtoken';
import { UserService } from 'services';
import Message from 'enums/message.enum';
import config from 'configs';

const { authTokenSecret } = config;

const jwtErrorNames = ['TokenExpiredError', 'JsonWebTokenError', 'NotBeforeError'];

function getTokenFromHeaders(req) {
  const { authorization } = req.headers;

  if (!authorization) {
    throw new Error(Message.TOKEN_IS_REQUIRED);
  }

  const parts = authorization.split(' ');

  if (parts.length !== 2) {
    throw new Error(Message.TOKEN_IS_INVALID);
  }

  if (parts[0] === 'Bearer') {
    return parts[1];
  }

  throw new Error(Message.TOKEN_IS_INVALID);
}

export default async function (req, res, next) {
  try {
    const token = getTokenFromHeaders(req);

    const decoded = jwt.verify(token, authTokenSecret);

    const { user: userData } = decoded;

    req.user = await UserService.findUserById(userData.id);

    return next();
  } catch (e) {
    if (jwtErrorNames.includes(e.name)) {
      return res.build.unauthorized(Message.TOKEN_IS_INVALID);
    }

    switch (e.message) {
      case Message.USER_NOT_FOUND:
      case Message.TOKEN_IS_INVALID:
      case Message.TOKEN_IS_REQUIRED: {
        return res.build.unauthorized(e.message);
      }

      default: {
        return next(e);
      }
    }
  }
}
