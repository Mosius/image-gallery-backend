class ResponseBuilder {
  constructor(response) {
    this.response = response;
  }

  success(data) {
    return this.response.status(200).send(data);
  }

  created() {
    return this.response.status(201).end();
  }

  noContent() {
    return this.response.status(204).end();
  }

  badRequest(error) {
    const res = {
      message: global.translate(error) || error,
    };

    return this.response.status(400).send(res);
  }

  unauthorized(error) {

    const res = {
      message: global.translate(error) || error,
    };

    return this.response.status(401).send(res);
  }

  forbidden(error) {
    const res = {
      message: global.translate(error) || error,
    };

    return this.response.status(403).send(res);
  }

  notFound(error) {
    const res = {
      message: global.translate(error) || error,
    };

    return this.response.status(404).send(res);
  }

  unprocessableEntity(error) {
    const res = {
      message: global.translate(error) || error,
    };

    return this.response.status(422).send(res);
  }

  paymentRequired(error) {
    const res = {
      message: global.translate(error) || error,
    };

    return this.response.status(402).send(res);
  }

  internal(error) {
    const res = {
      message: global.translate(error) || error,
    };

    return this.response.status(500).send(res);
  }
}

export default function (req, res, next) {
  try {
    res.build = new ResponseBuilder(res);
    return next();
  } catch (error) {
    return next(error);
  }
}
