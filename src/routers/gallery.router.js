import express from 'express';
import validate from 'express-validation';
import { UserSession } from 'middleware';
import {
  GalleryService,
  GalleryPostService,
  LikeService,
} from 'services';
import {
  galleryCreateNew,
  galleryGetList,
  gallerySendPost,
  galleryGetPosts,
  galleryPublishPost,
  galleryLikePost,
  galleryUnlikePost,
} from 'routers/validators';
import {
  transformGallery,
  transformGalleryPost,
} from 'routers/transformers';

const router = express.Router();

async function createNewGallery(req, res, next) {
  try {
    const {
      title,
      description,
      tags,
      meta,
    } = req.body;

    const { _id: owner } = req.user;

    const gallery = await GalleryService.createGallery({
      owner,
      title,
      description,
      tags,
      meta,
    });

    const result = transformGallery(gallery);
    return res.build.success({ gallery: result });
  } catch (e) {
    return next(e);
  }
}

async function getGalleriesList(req, res, next) {
  try {
    const { lastId, status } = req.query;

    const galleries = await GalleryService.getGalleries({ status, lastId });

    const result = galleries.map(transformGallery);
    return res.build.success({ galleries: result });
  } catch (e) {
    return next(e);
  }
}

async function sendGalleryPost(req, res, next) {
  try {
    const { id: galleryId } = req.params;
    const { photo: photoId } = req.body;

    const post = await GalleryPostService.sendGalleryPost({ photoId, galleryId });

    const result = transformGalleryPost(post);
    return res.build.success({ post: result });
  } catch (e) {
    return next(e);
  }
}

async function getGalleryPosts(req, res, next) {
  try {
    const { _id: userId } = req.user;
    const { id: galleryId } = req.params;
    const { lastId } = req.query;

    const posts = await GalleryPostService.getGalleryPosts({ galleryId, lastId, userId });

    const result = posts.map(transformGalleryPost);
    return res.build.success({ posts: result });
  } catch (e) {
    return next(e);
  }
}

async function publishGalleryPost(req, res, next) {
  try {
    const {
      postId,
      id: galleryId,
    } = req.params;

    const post = await GalleryPostService.publishPost({ postId, galleryId });

    const result = transformGalleryPost(post);
    return res.build.success({ post: result });
  } catch (e) {
    return next(e);
  }
}

async function likeGalleryPost(req, res, next) {
  try {
    const {
      postId,
      id: galleryId,
    } = req.params;
    const { _id: userId } = req.user;

    const result = await LikeService.likeGalleryPost({ postId, userId, galleryId });

    return res.build.success({ new: result });
  } catch (e) {
    return next(e);
  }
}

async function unlikeGalleryPost(req, res, next) {
  try {
    const {
      postId,
      id: galleryId,
    } = req.params;
    const { _id: userId } = req.user;

    const result = await LikeService.unlikeGalleryPost({ postId, userId, galleryId });

    return res.build.success({ new: result });
  } catch (e) {
    return next(e);
  }
}

router.get(
  '/',
  UserSession,
  validate(galleryGetList),
  getGalleriesList,
);

router.post(
  '/',
  UserSession,
  validate(galleryCreateNew),
  createNewGallery,
);

router.post(
  '/:id/posts',
  UserSession,
  validate(gallerySendPost),
  sendGalleryPost,
);

router.get(
  '/:id/posts',
  UserSession,
  validate(galleryGetPosts),
  getGalleryPosts,
);

router.patch(
  '/:id/posts/:postId/publish',
  UserSession,
  validate(galleryPublishPost),
  publishGalleryPost,
);

router.put(
  '/:id/posts/:postId/like',
  UserSession,
  validate(galleryLikePost),
  likeGalleryPost,
);

router.delete(
  '/:id/posts/:postId/like',
  UserSession,
  validate(galleryUnlikePost),
  unlikeGalleryPost,
);

export default router;
