import { transform } from 'transformobject';
import transformUser from './user.transformer';

const rules = {
  id: '_id',
  owner: obj => transformUser(obj.owner),
  title: 'title',
  description: 'description',
  tags: 'tags',
  meta: 'meta',
  status: 'status',
  createdAt: 'createdAt',
};

export default obj => transform(obj, rules);
