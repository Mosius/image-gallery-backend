import { transform } from 'transformobject';
import transformPhoto from './photo.transformer';

const rules = {
  id: '_id',
  photo: obj => transformPhoto(obj.photo),
  likes: 'likes',
  liked: 'liked',
  status: 'status',
  createdAt: 'createdAt',
};

export default obj => transform(obj, rules);
