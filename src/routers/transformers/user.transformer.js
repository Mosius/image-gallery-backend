import { transform } from 'transformobject';

const rules = {
  id: '_id',
  firstName: 'firstName',
  lastName: 'lastName',
  gender: 'gender',
};

export default obj => transform(obj, rules);
