import urlJoin from 'url-join';
import config from 'configs';
import { transform } from 'transformobject';
import transformUser from './user.transformer';

const rules = {
  id: '_id',
  url: obj => urlJoin(config.mediaUrl, obj.path),
  owner: obj => transformUser(obj.owner),
  description: 'description',
  tags: 'tags',
  meta: 'meta',
  createdAt: 'createdAt',
};

export default obj => transform(obj, rules);
