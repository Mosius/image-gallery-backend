import Joi from 'joi';
import Gender from 'enums/gender.enum';

const nameRegex =  /^(?:((([^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]'’,\-.\s])){1,}(['’,\-\.]){0,1}){2,}(([^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]'’,\-. ]))*(([ ]+){0,1}(((([^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]'’,\-\.\s])){1,})(['’\-,\.]){0,1}){2,}((([^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]'’,\-\.\s])){2,})?)*)$/; // eslint-disable-line max-len

export default {
  body: {
    email: Joi.string().email({ minDomainSegments: 2 }).required(),
    password: Joi.string().regex(/^.{6,}$/).required(),
    firstName: Joi.string().regex(nameRegex).required(),
    lastName: Joi.string().regex(nameRegex).required(),
    gender: Joi.string().only(Object.values(Gender)).required(),
  },
};
