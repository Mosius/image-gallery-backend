import Joi from 'joi';
import Status from 'enums/status.enum';

Joi.objectId = require('joi-objectid')(Joi);

export default {
  query: {
    lastId: Joi.objectId().optional(),
    status: Joi.array().items(Joi.string().valid(Object.values(Status.GALLERY))).optional(),
  },
};
