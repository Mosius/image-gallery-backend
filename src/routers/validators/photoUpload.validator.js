import Joi from 'joi';

export default {
  body: {
    description: Joi.string().optional(),
    tags: Joi.array().items(Joi.string()).optional(),
    meta: Joi.any().optional(),
  },
  files: {
    photo: Joi.any().required(),
  },
};
