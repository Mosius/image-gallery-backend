import Joi from 'joi';

Joi.objectId = require('joi-objectid')(Joi);

export default {
  query: {
    lastId: Joi.objectId().optional(),
  },
};
