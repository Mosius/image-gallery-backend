import userRegistration from './userRegistration.validator';
import userLogin from './userLogin.validator';
import userGalleries from './userGalleries.validator';
import galleryCreateNew from './galleryCreateNew.validator';
import galleryGetList from './galleryGetList.validator';
import gallerySendPost from './gallerySendPost.validator';
import galleryGetPosts from './galleryGetPosts.validator';
import galleryPublishPost from './galleryPublishPost.validator';
import galleryLikePost from './galleryLikePost.validator';
import galleryUnlikePost from './galleryUnlikePost.validator';
import photoGet from './photoGet.validator';
import photoUpload from './photoUpload.validator';

export {
  userRegistration,
  userLogin,
  userGalleries,
  galleryCreateNew,
  galleryGetList,
  gallerySendPost,
  galleryGetPosts,
  galleryPublishPost,
  galleryLikePost,
  galleryUnlikePost,
  photoGet,
  photoUpload,
};
