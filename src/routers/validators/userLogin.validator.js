import Joi from 'joi';

export default {
  body: {
    email: Joi.string().email({ minDomainSegments: 2 }).required(),
    password: Joi.string().regex(/^.{6,}$/).required(),
  },
};
