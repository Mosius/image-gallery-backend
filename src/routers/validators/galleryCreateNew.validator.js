import Joi from 'joi';

Joi.objectId = require('joi-objectid')(Joi);

export default {
  body: {
    title: Joi.string().required(),
    description: Joi.string().optional(),
    tags: Joi.array().items(Joi.string()).optional(),
    meta: Joi.any().optional(),
  },
};
