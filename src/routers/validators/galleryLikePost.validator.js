import Joi from 'joi';

Joi.objectId = require('joi-objectid')(Joi);

export default {
  params: {
    id: Joi.objectId().required(),
    postId: Joi.objectId().required(),
  },
};
