import express from 'express';
import validate from 'express-validation';
import { UserSession } from 'middleware';
import {
  UserService,
  GalleryService,
} from 'services';
import {
  userRegistration,
  userLogin,
  userGalleries,
} from 'routers/validators';
import {
  transformUser,
  transformGallery,
} from 'routers/transformers';

const router = express.Router();

async function register(req, res, next) {
  try {
    const {
      email,
      password,
      firstName,
      lastName,
      gender,
    } = req.body;

    const user = await UserService.registerUser({
      email,
      password,
      firstName,
      lastName,
      gender,
    });

    const result = transformUser(user);
    return res.build.success({ user: result });
  } catch (e) {
    return next(e);
  }
}

async function login(req, res, next) {
  try {
    const {
      email,
      password,
    } = req.body;

    const token = await UserService.authenticateUser({ email, password });

    return res.build.success({ token });
  } catch (e) {
    return next(e);
  }
}

async function getGalleriesList(req, res, next) {
  try {
    const { lastId } = req.query;
    const { _id: userId } = req.user;

    const galleries = await GalleryService.getUserGalleries({ userId, lastId });

    const result = galleries.map(transformGallery);
    return res.build.success({ galleries: result });
  } catch (e) {
    return next(e);
  }
}

async function getUserProfile(req, res, next) {
  try {
    const { _id: userId } = req.user;

    const user = await UserService.findUserById(userId);

    const result = transformUser(user);
    return res.build.success({ user: result });
  } catch (e) {
    return next(e);
  }
}


router.post(
  '/',
  validate(userRegistration),
  register,
);

router.post(
  '/login',
  validate(userLogin),
  login,
);

router.get(
  '/galleries',
  UserSession,
  validate(userGalleries),
  getGalleriesList,
);

router.get(
  '/profile',
  UserSession,
  getUserProfile,
);

export default router;
