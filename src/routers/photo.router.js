import express from 'express';
import validate from 'express-validation';
import { UserSession } from 'middleware';
import { PhotoService } from 'services';
import {
  photoGet,
  photoUpload,
} from 'routers/validators';
import { transformPhoto } from 'routers/transformers';

const router = express.Router();

async function uploadPhoto(req, res, next) {
  try {
    const {
      description,
      tags,
      meta,
    } = req.body;

    const { photo: photoFile } = req.files;

    const { _id: owner } = req.user;

    const photo = await PhotoService.createAndSavePhoto({
      owner,
      description,
      tags,
      meta,
      photoFile,
    });

    const result = transformPhoto(photo);
    return res.build.success({ photo: result });
  } catch (e) {
    return next(e);
  }
}

async function getUserPhotosList(req, res, next) {
  try {
    const { lastId } = req.query;
    const { _id: userId } = req.user;

    const photos = await PhotoService.getUserPhotos({ userId, lastId });

    const result = photos.map(transformPhoto);
    return res.build.success({ photos: result });
  } catch (e) {
    return next(e);
  }
}

router.get(
  '/',
  UserSession,
  validate(photoGet),
  getUserPhotosList,
);

router.post(
  '/',
  UserSession,
  validate(photoUpload),
  uploadPhoto,
);

export default router;
