import express from 'express';
import userRouter from './user.router';
import photoRouter from './photo.router';
import galleryRouter from './gallery.router';


const router = express.Router();

router.use('/users', userRouter);
router.use('/photos', photoRouter);
router.use('/galleries', galleryRouter);

export default router;
