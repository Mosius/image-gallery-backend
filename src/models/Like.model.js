import mongoose from 'connections/mongoose';

const LikeSchema = new mongoose.Schema(
  {
    galleryPost: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'GalleryPost',
      required: true,
      index: true,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
      index: true,
    },
    gallery: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Gallery',
      required: true,
    },
  },
  { timestamps: true },
);

const Like = mongoose.model('Like', LikeSchema);

export default Like;
