import mongoose from 'connections/mongoose';
import Status from 'enums/status.enum';

const GalleryPost = new mongoose.Schema(
  {
    photo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Photo',
      required: true,
    },
    likes: {
      type: Number,
      required: true,
      default: 0,
    },
    status: {
      type: String,
      required: true,
      enum: Object.values(Status.GALLERY_POST),
      default: Status.GALLERY_POST.REVIEW,
    },
  },
  { timestamps: true },
);

const GallerySchema = new mongoose.Schema(
  {
    owner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
      index: true,
    },
    title: {
      type: String,
      required: true,
    },
    description: String,
    tags: [String],
    posts: {
      type: [GalleryPost],
      required: true,
      default: [],
    },
    meta: Object,
    status: {
      type: String,
      required: true,
      enum: Object.values(Status.GALLERY),
      default: Status.GALLERY.ACTIVE,
    },
  },
  { timestamps: true },
);

const Gallery = mongoose.model('Gallery', GallerySchema);

export default Gallery;
