import mongooseBcrypt from 'mongoose-bcrypt';
import mongoose from 'connections/mongoose';
import Status from 'enums/status.enum';
import Gender from 'enums/gender.enum';

const UserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      trim: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    firstName: {
      type: String,
      required: true,
      trim: true,
    },
    lastName: {
      type: String,
      required: true,
      trim: true,
    },
    gender: {
      type: String,
      enum: Object.values(Gender),
    },
    status: {
      type: String,
      required: true,
      enum: Object.values(Status.USER),
      default: Status.USER.ACTIVE,
    },
  },
  { timestamps: true },
);

// Add { password: String } to schema
UserSchema.plugin(mongooseBcrypt, { fields: ['password'], rounds: 8 });

const User = mongoose.model('User', UserSchema);

export default User;
