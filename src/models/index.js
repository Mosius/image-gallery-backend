import User from './User.model';
import Photo from './Photo.model';
import Gallery from './Gallery.model';
import Like from './Like.model';

export {
  User,
  Photo,
  Gallery,
  Like,
};
