import mongoose from 'connections/mongoose';
import Status from 'enums/status.enum';

const PhotoSchema = new mongoose.Schema(
  {
    path: {
      type: String,
      required: true,
    },
    owner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
      index: true,
    },
    description: String,
    tags: [String],
    meta: Object,
    status: {
      type: String,
      required: true,
      enum: Object.values(Status.PHOTO),
      default: Status.PHOTO.PUBLISHED,
    },
  },
  { timestamps: true },
);

const Photo = mongoose.model('Photo', PhotoSchema);

export default Photo;
