import jwt from 'jsonwebtoken';
import config from 'configs';
import Message from 'enums/message.enum';
import { User } from 'models';


export default {
  async findUserById(userId) {
    const user = await User.findById(userId);

    if (!user) {
      throw Message.USER_NOT_FOUND;
    }

    return user;
  },

  async findUserByEmail(email) {
    const user = await User.findOne({ email });

    if (!user) {
      throw Message.USER_NOT_FOUND;
    }

    return user;
  },

  async registerUser(
    {
      email,
      password,
      firstName,
      lastName,
      gender,
    },
  ) {
    return User.create({
      email,
      password,
      firstName,
      lastName,
      gender,
    });
  },

  async authenticateUser(
    {
      email,
      password,
    },
  ) {
    const user = await User.findOne({ email });

    if (!user) {
      throw Message.USER_AUTHENTICATION_FAILED;
    }

    const isPasswordVerified = await user.verifyPassword(password);

    if (!isPasswordVerified) {
      throw Message.USER_AUTHENTICATION_FAILED;
    }

    const { authTokenSecret } = config;

    return jwt.sign({ user: { id: user._id } }, authTokenSecret);
  },
};
