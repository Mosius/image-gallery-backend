import UserService from './user.service';
import PhotoService from './photo.service';
import GalleryService from './gallery.service';
import GalleryPostService from './galleryPost.service';
import LikeService from './like.service';

export {
  UserService,
  PhotoService,
  GalleryService,
  GalleryPostService,
  LikeService,
};
