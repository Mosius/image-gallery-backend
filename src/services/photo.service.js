import fs from 'fs-extra-promise';
import urlJoin from 'url-join';
import config from 'configs';
import { Photo } from 'models';

export default {
  async createAndSavePhoto(
    {
      photoFile,
      owner,
      description,
      tags,
      meta,
    },
  ) {
    const photo = new Photo({
      owner,
      description,
      tags,
      meta,
    });

    const photoFileName = `${photo._id}.jpeg`; // TODO put the hashed value here

    const userFolder = `/${owner}`; // TODO put the hashed value here

    const photoPath = urlJoin(config.mediaPath, userFolder);

    const userFolderExists = await fs.existsAsync(photoPath);
    if (!userFolderExists) {
      await fs.mkdirsAsync(photoPath);
    }

    photo.path = urlJoin(userFolder, photoFileName);

    await fs.writeFileAsync(urlJoin(photoPath, photoFileName), photoFile.data);

    await Photo.populate(photo, 'owner');
    return photo.save();
  },

  async getUserPhotos(
    {
      userId,
      lastId,
    },
  ) {
    const query = {
      owner: userId,
    };

    if (lastId) {
      const last = await Photo.findById(lastId, { createdAt: 1 });

      if (last) {
        query.$or = [
          { createdAt: { $lt: last.createdAt } },
          {
            $and: [
              { createdAt: { $lte: last.createdAt } },
              { _id: { $lt: lastId } },
            ],
          },
        ];
      }
    }

    const { pageSize } = config;

    return Photo.find(query).sort([
      ['createdAt', -1],
      ['_id', -1],
    ]).limit(pageSize)
      .populate('owner');
  },
};
