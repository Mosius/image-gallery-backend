import { Gallery } from 'models';
import config from 'configs';
import Message from 'enums/message.enum';

export default {
  async createGallery(
    {
      owner,
      title,
      description,
      tags,
      meta,
    },
  ) {
    const gallery = await Gallery.create({
      owner,
      title,
      description,
      tags,
      meta,
    });

    await Gallery.populate(gallery, 'owner');

    return gallery;
  },

  async getUserGalleries(
    {
      userId,
      lastId,
    },
  ) {
    const query = {
      owner: userId,
    };

    if (lastId) {
      const last = await Gallery.findById(lastId, { createdAt: 1 });

      if (!last) {
        throw new Error(Message.GALLERY_NOT_FOUND);
      }

      query.$or = [
        { createdAt: { $lt: last.createdAt } },
        {
          $and: [
            { createdAt: { $lte: last.createdAt } },
            { _id: { $lt: lastId } },
          ],
        },
      ];
    }

    const { pageSize } = config;

    return Gallery.find(query).sort([
      ['createdAt', -1],
      ['_id', -1],
    ]).limit(pageSize)
      .populate('owner')
      .select('-posts')
      .lean();
  },

  async getGalleries(
    {
      status,
      lastId,
    },
  ) {
    const query = {};

    if (status) {
      query.status = { $in: status };
    }

    if (lastId) {
      const last = await Gallery.findById(lastId, { createdAt: 1 });

      if (!last) {
        throw new Error(Message.GALLERY_NOT_FOUND);
      }

      query.$or = [
        { createdAt: { $lt: last.createdAt } },
        {
          $and: [
            { createdAt: { $lte: last.createdAt } },
            { _id: { $lt: lastId } },
          ],
        },
      ];
    }

    const { pageSize } = config;

    return Gallery.find(query).sort([
      ['createdAt', -1],
      ['_id', -1],
    ]).limit(pageSize)
      .populate('owner')
      .select('-posts')
      .lean();
  },
};
