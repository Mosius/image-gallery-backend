import mongoose from 'connections/mongoose';
import config from 'configs';
import {
  Gallery,
  Photo,
} from 'models';
import { LikeService } from 'services';
import Status from 'enums/status.enum';
import Message from 'enums/message.enum';

const { pageSize } = config;

export default {
  async sendGalleryPost(
    {
      photoId,
      galleryId,
    },
  ) {
    const photo = await Photo.findOne({
      _id: photoId,
      status: Status.PHOTO.PUBLISHED,
    });

    if (!photo) {
      throw new Error(Message.PHOTO_NOT_FOUND);
    }

    const gallery = await Gallery.findOne({
      _id: galleryId,
      status: Status.GALLERY.ACTIVE,
    });

    if (!gallery) {
      throw new Error(Message.GALLERY_NOT_FOUND);
    }

    const post = await Gallery.countDocuments({
      _id: galleryId,
      posts: { $elemMatch: { photo: photoId } },
    });

    if (post) {
      throw new Error(Message.PHOTO_ALREADY_IN_GALLERY);
    }

    const galleryPostStatus = String(photo.owner) === String(gallery.owner) ? Status.GALLERY_POST.PUBLISHED : Status.GALLERY_POST.REVIEW;
    const galleryPost = {
      photo: photoId,
      status: galleryPostStatus,
    };

    gallery.posts.push(galleryPost);

    await gallery.save();

    await Gallery.populate(gallery, 'posts.photo');
    await Gallery.populate(gallery, 'posts.photo.owner');

    const postIndex = gallery.posts.length - 1;
    return gallery.posts[postIndex];
  },

  async getGalleryPosts(
    {
      galleryId,
      lastId,
      userId,
    },
  ) {
    const gallery = await Gallery.countDocuments({ _id: galleryId });

    if (!gallery) {
      throw new Error(Message.GALLERY_NOT_FOUND);
    }

    const ownGallery = await Gallery.countDocuments({ _id: galleryId, owner: userId });

    const pipeline = [
      { $match: { _id: mongoose.Types.ObjectId(galleryId) } },
      { $unwind: '$posts' },
      { $replaceRoot: { newRoot: '$posts' } },
    ];

    if (!ownGallery) {
      pipeline.push({ $match: { status: Status.GALLERY_POST.PUBLISHED } });
    }

    if (lastId) {
      const { posts } = await Gallery.findOne(
        { _id: galleryId },
        { posts: { $elemMatch: { _id: lastId } }, 'posts.createdAt': true },
      );
      const last = posts[0];

      if (!last) {
        throw new Error(Message.POST_NOT_FOUND);
      }

      pipeline.push({
        $match: {
          $or: [
            { createdAt: { $lt: last.createdAt } },
            {
              $and: [
                { createdAt: { $lte: last.createdAt } },
                { _id: { $lt: lastId } },
              ],
            },
          ],
        },
      });
    }

    pipeline.push(
      { $sort: { createdAt: -1, _id: -1 } },
      { $limit: pageSize },
      {
        $lookup: {
          from: 'photos',
          localField: 'photo',
          foreignField: '_id',
          as: 'photo',
        },
      },
      {
        $unwind: {
          path: '$photo',
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'photo.owner',
          foreignField: '_id',
          as: 'photo.owner',
        },
      },
      {
        $addFields: {
          'photo.owner': { $arrayElemAt: ['$photo.owner', 0] },
        },
      },
    );

    const posts = await Gallery.aggregate(pipeline);

    const postsId = posts.map(it => it._id);
    const likes = await LikeService.getUserLikesOnPosts({ userId, postsId });

    posts.forEach((it) => {
      it.liked = likes.find(like => String(like.galleryPost) === String(it._id)) != null;
    });

    return posts;
  },

  async publishPost(
    {
      postId,
      galleryId,
    },
  ) {
    const result = await Gallery.update(
      {
        _id: galleryId,
        'posts._id': postId,
      },
      {
        $set: {
          'posts.$.status': Status.GALLERY_POST.PUBLISHED,
        },
      },
    );

    if (!result.n) {
      throw new Error(Message.POST_NOT_FOUND);
    }

    const { posts } = await Gallery.findOne(
      { _id: galleryId },
      { posts: { $elemMatch: { _id: postId } } },
    ).populate({ path: 'posts.photo', populate: { path: 'owner' } });

    return posts[0];
  },
};
