import {
  Like,
  Gallery,
} from 'models';

export default {
  async likeGalleryPost(
    {
      postId,
      userId,
      galleryId,
    },
  ) {
    const { nModified } = await Like.update(
      { user: userId, galleryPost: postId, gallery: galleryId },
      { user: userId, galleryPost: postId, gallery: galleryId },
      { upsert: true },
    );

    const result = nModified === 0;

    if (result) {
      await Gallery.update(
        {
          _id: galleryId,
          'posts._id': postId,
        },
        {
          $inc: {
            'posts.$.likes': 1,
          },
        },
      );
    }

    return result;
  },

  async unlikeGalleryPost(
    {
      postId,
      userId,
      galleryId,
    },
  ) {
    const { deletedCount } = await Like.deleteOne(
      { user: userId, galleryPost: postId, gallery: galleryId },
    );

    const result = deletedCount === 1;

    if (result) {
      await Gallery.update(
        {
          _id: galleryId,
          'posts._id': postId,
        },
        {
          $inc: {
            'posts.$.likes': -1,
          },
        },
      );
    }

    return result;
  },

  async getUserLikesOnPosts(
    {
      userId,
      postsId,
    },
  ) {
    return Like.find({ user: userId, galleryPost: { $in: postsId } });
  },
};
