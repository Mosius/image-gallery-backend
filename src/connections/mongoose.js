import mongoose from 'mongoose';
import bluebird from 'bluebird';
import configs from 'configs';
import winston from 'connections/winstone';

const { connectionString, replicaSet } = configs.mongoose;

try {
  mongoose.connect(
    connectionString,
    { connectTimeoutMS: 4000, useNewUrlParser: true, replicaSet },
  ).then(() => {
    winston.info('Mongodb Connected');
  });
} catch (error) {
  winston.error('Mongodb Connection Error');
}

mongoose.connection.on('error', (error) => {
  winston.error(error);
  throw new Error(`Mongodb Unable To Connect To: ${connectionString}`);
});

mongoose.Promise = bluebird;

export default mongoose;
