import express from 'express';
import Promise from 'bluebird';
import i18n from 'i18n';
import path from 'path';
import bodyParser from 'body-parser';
import busboyBodyParser from 'busboy-body-parser';
import helmet from 'helmet';
import cors from 'cors';
import winstone from 'connections/winstone';
import configs from 'configs';
import routers from 'routers';
import { ResponseBuilder } from 'middleware';

global.Promise = Promise;

i18n.configure({
  locales: ['fa'],
  defaultLocale: 'fa',
  queryParameter: 'lang',
  directory: path.join(__dirname, '/locales'),
  register: global,
  indent: '  ',
  api: {
    __: 'translate',
  },
});

const app = express();

app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));
app.use(bodyParser.json({ limit: '1mb' }));
app.use(busboyBodyParser({ limit: '10mb' }));
app.use(helmet());
app.use(cors());

app.use(ResponseBuilder);

app.use('/uploads', express.static(configs.mediaPath));
app.use('/doc', express.static(path.join(__dirname, '../doc/api').toString()));

app.use(`/api/${configs.apiVersion}`, routers);

const server = app.listen(configs.apiPort, () => {
  const serverAddress = server.address();
  winstone.info(`API running at http://localhost:${serverAddress.port}/api/${configs.apiVersion}`);
});
