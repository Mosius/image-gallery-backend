module.exports = {
  extends: 'airbnb-base',
  rules: {
    'import/no-unresolved': 0,
    'no-underscore-dangle': 0,
    'max-len': [2, 150, 4, { 'ignoreUrls': true, 'ignoreStrings': true }],
  },
};
